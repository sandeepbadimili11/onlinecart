import React, { Component } from 'react';
import { CardDeck, Card, Button } from 'react-bootstrap';
class Products extends Component {

    state = {
        products: []
    }
    loadProducts = () => {
        fetch('http://127.0.0.1:8000/routers/products/', {
            method: 'GET',
            body: JSON.stringify(this.state.credentials)
        })
            .then(data => data.json())
            .then(
                data => {
                    // console.log(data);
                    this.setState({ products: data })
                }
            )
            .catch(error => console.error(error))
    }

    render() {
        return (
            <div className="container row" >

                {this.loadProducts()}

                {this.state.products.map((items) => (
                    <CardDeck style={{ width: '22rem' }}>
                        <p key={items.id}></p>
                        <Card style={{ margin: '10px' }}>
                            <Card.Img variant="top" src={items.image} />
                            <Card.Body>
                                <Card.Title>{items.Title}</Card.Title>
                                <Card.Text>
                                    Description:{items.Description}
                                </Card.Text>
                                        Price:₹{items.price}
                                <div>
                                    <Button variant="primary" size="md">
                                        Buy Now
                                    </Button>{' '}
                                    <Button variant="primary" size="md">
                                        Add to
                                     </Button>
                                </div>
                            </Card.Body>
                            <Card.Footer>
                                <small className="text-muted"></small>
                            </Card.Footer>
                        </Card>


                    </CardDeck>
                ))}
            </div>


        );

    }
}

export default Products;