from django.contrib.auth.models import User
from rest_framework.authentication import TokenAuthentication
from Ecart.models import Products, orders, ordersItems
from Ecart.serializers import ProductSerializer,Orders_itemSerializer,OrdersSerializer,CreateUserSerializer,LoginUserSerializer
from rest_framework import generics
from rest_framework.permissions import IsAdminUser, IsAuthenticated, IsAuthenticatedOrReadOnly
from django.contrib.auth import logout, login, authenticate
from rest_framework import viewsets
from rest_framework.filters import SearchFilter,OrderingFilter



class Detailsproduct(viewsets.ModelViewSet):
    # def get_queryset(self):
    #    return Products.objects.filter(user=self.request.user)

    queryset=Products.objects.all()  
    serializer_class = ProductSerializer
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('id', 'Title')

    # authentication_classes = [TokenAuthentication]   # get and Post 
    # permission_classes = [IsAuthenticated]

    # def perform_create(self, serializer):
    #     serializer.save(user=self.request.user)



class DetailsproductD(generics.RetrieveUpdateDestroyAPIView):
    # def get_queryset(self):
    #    return Products.objects.filter(user=self.request.user)

    queryset=Products.objects.all()
    serializer_class = ProductSerializer
    
    # authentication_classes = [BasicAuthentication]
    # permission_classes = [IsAuthenticatedOrReadOnly]
    # permission_classes = [IsAuthenticated]





class Detailsorders(generics.ListCreateAPIView):
    queryset = orders.objects.all()
    serializer_class = OrdersSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]


class DetailsorderD(generics.RetrieveUpdateDestroyAPIView):
    queryset = orders.objects.all()
    serializer_class = OrdersSerializer
    
    # authentication_classes = [BasicAuthentication]
    # permission_classes = [IsAuthenticatedOrReadOnly]


class Detailsorderitems(generics.ListCreateAPIView):
    queryset = ordersItems.objects.all()
    serializer_class = Orders_itemSerializer
    # authentication_classes = [BasicAuthentication]
    # permission_classes = [IsAuthenticated]


class DetailsorderitemsD(generics.RetrieveUpdateDestroyAPIView):
    queryset = orders.objects.all()
    serializer_class = Orders_itemSerializer
    # authentication_classes = [BasicAuthentication]
    # permission_classes = [IsAuthenticatedOrReadOnly]
# class user(generics.ListCreateAPIView):
#     queryset = User.objects.all()
#     serializer_class = CreateUserSerializer

#     def perform_create(self, serializer):
#         serializer.save(user=self.request.user)
class userviews(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class=CreateUserSerializer


# class userDetails(generics.RetrieveUpdateDestroyAPIView):
#     queryset = User.objects.all()
#     serializer_class = CreateUserSerializer

class userLogin(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class=LoginUserSerializer

# class Logout(generics.ListCreateAPIView):
#     def get(self, request, format=None):
#         # simply delete the token to force a login
#         request.user.delete()
#         return Response(status=status.HTTP_200_OK)
# class userDeatials(generics.RetrieveUpdateDestroyAPIView):
#     queryset = User.objects.all()
#     serializer_class = OrdersSerializer

   
