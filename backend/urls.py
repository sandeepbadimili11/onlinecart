"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from backend import rest_views
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework import routers
from backend.rest_views import userviews, Detailsproduct, userLogin
router = routers.DefaultRouter()
router.register('account/register', userviews)
router.register('products', Detailsproduct)
# router.register('login', userLogin)
# router.register('login', obtain_auth_token)


urlpatterns = [
    path('admin/', admin.site.urls),
    
    
    # path("api/products/", rest_views.Detailsproduct.as_view()),
    path("api/products/<int:pk>/", rest_views.DetailsproductD.as_view()),


    path("api/orders/", rest_views.Detailsorders.as_view()),
    path("api/orders/<int:pk>/", rest_views.DetailsorderD.as_view()),


    path("api/orderitems/", rest_views.Detailsorderitems.as_view()),
    path("orderitems/<int:pk>/", rest_views.DetailsorderitemsD.as_view()),
    

    path('routers/',include(router.urls)),

  


     
    path('login/', obtain_auth_token),
    
    # path("register/<str:username>/", rest_views.userDetails.as_view()),
]
