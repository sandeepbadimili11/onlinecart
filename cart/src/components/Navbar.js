import React from 'react';
import { Link } from 'react-router-dom'
 const Navbar = ()=>{
    return(
            <nav className="nav-wrapper color orange">
                <div className="container-fluid">
                    <Link to="/" className="brand-logo left" >Shopping</Link>
                    
                <ul className="right">
                    <li></li><input type="search" placeholder="search items" value="search" /><li/>
                    <button type="search" value="search"/>
                        <li><Link to="/">Shop</Link></li>
                        <li><Link to="/cart">My cart</Link></li>
                        <li><Link to="/cart"><i className="material-icons">shopping_cart</i></Link></li>
                    </ul>
                </div>
            </nav>
   
            
        
    )
}

export default Navbar;