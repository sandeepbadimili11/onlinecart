from django.contrib import admin
from .models import Products,orders,ordersItems


class ProductDetails(admin.ModelAdmin):
    list_display= ('id', 'Title', 'Description', 'image',
                    'price', 'Created_At', 'Updated_At')

class orderDetails(admin.ModelAdmin):
    list_display = ('id', 'userId', 'Total', 'Created_At', 'Updated_At', 'status', 'mode_of_payment')
    
class orderitems(admin.ModelAdmin):
    list_display= ('id','orderId','productName','Quantity','price')
# Register your models here.
admin.site.register(Products,ProductDetails)
admin.site.register(orders,orderDetails)
admin.site.register(ordersItems,orderitems)

